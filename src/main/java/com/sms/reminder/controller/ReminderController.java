package com.sms.reminder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sms.reminder.model.AuthenticationRequest;
import com.sms.reminder.model.AuthenticationResponse;
import com.sms.reminder.model.ReminderInfo;
import com.sms.reminder.service.ReminderService;
import com.sms.reminder.service.ReminderUserDetailService;
import com.sms.reminder.util.ReminderJwtUtil;

@CrossOrigin("*")
@RestController
public class ReminderController {
	@Autowired
    private ReminderService reminderService;	

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ReminderJwtUtil jwtTokenUtil;

	@Autowired
	private ReminderUserDetailService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
		try {
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword())
			);
		}
		catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}
	
    @PostMapping("/reminder")
    public ResponseEntity<ReminderInfo> saveOrder(@RequestBody ReminderInfo remindrInfo) {
        return new ResponseEntity<>(reminderService.createReminderInfo(remindrInfo), HttpStatus.OK);
    }

    @GetMapping("/reminder")
    public ResponseEntity<List<ReminderInfo>> getAllReminderInfo() {
        return new ResponseEntity<>(reminderService.getAllReminderInfo(), HttpStatus.OK);
    }

    @GetMapping("/reminder/{id}")
    public ResponseEntity<ReminderInfo> getOrderById(@PathVariable Long id) {
        return new ResponseEntity<>(reminderService.getReminderInfoById(id), HttpStatus.OK);

    }

    @DeleteMapping("/reminder/{id}")
    public boolean deleteOrder(@PathVariable String id) {
        return reminderService.deleteReminderInfo(id);
    }
}
